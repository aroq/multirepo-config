executePipeline {
    pipeline =
        [
            'init': [
                [
                    action: 'Docman.config',
                ],
            ],
            'build': [
                [
                    action: 'Docman.deploy',
                ],
                [
                    action: 'CustomAction1.perform',
                ]
            ],
            'stage deploy': [
                [
                    action: 'Druflow.deployFlow',
                    params: [
                    ]
                ],
            ],
            'stage smoke testing': [
                [
                    action: 'Behat.perform',
                    params: [
                      tags: '@smoke'
                    ]
                ],
            ],
            'stage acceptance testing': [
                [
                    action: 'Behat.perform',
                    params: [
                      tags: '@acceptance'
                    ]
                ],
            ],
            'prod deploy': [
                [
                    action: 'Druflow.deployFlow',
                    params: [
                      deployFlowEnvironment: 'test',
                      //deployFlowConfirm: [
                      // environment: 'test',
                      //  message: 'Deploy to Production?'
                      //]
                    ]
                ],
            ],
            'prod smoke testing': [
                [
                    action: 'Behat.perform',
                    params: [
                      testEnvironment: 'test',
                      tags: '@smoke'
                    ]
                ],
            ],
            'prod acceptance testing': [
                [
                    action: 'Behat.perform',
                    params: [
                      testEnvironment: 'test',
                      tags: '@acceptance'
                    ]
                ],
            ],
            'finalize': [
                [
                    action: 'Publish.junit',
                ],
            ]
        ]
}
