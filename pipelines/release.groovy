node("ondemand") {
    checkout scm
    withCredentials([[$class: 'FileBinding', credentialsId: 'id_rsa', variable: 'ID_RSA_FILE']]) {

        //def drudock = docker.build 'drudock:snapshot', 'docroot/config'
        def drudock = docker.image('aroq/drudock:1.0.1')
        drudock.pull()
        drudock.inside {
            sh("cp ${env.ID_RSA_FILE} /root/.ssh/")
            executePipeline {
                noNode = true
                pipeline =
                    [
                        'build': [
                            [
                                action: 'Docman.config',
                            ],
                            [
                                action: 'Docman.jsonConfig',
                            ],
                            [
                                action: 'Docman.deploy',
                            ],
                        ],
                        'stage prepare': [
                            [
                                action: 'Druflow.copySite',
                                params: [
                                  fromEnvironment: 'test',
                                  toEnvironment: 'dev',
                                  db: 'aroq',
                                ]
                            ],
                        ],
                        'stage deploy': [
                            [
                                action: 'Druflow.deployFlow',
                                params: [
                                  deployFlowEnvironment: 'dev',
                                ]
                            ],
                        ],
                        'stage smoke testing': [
                            [
                                action: 'Behat.perform',
                                params: [
                                  testEnvironment: 'dev',
                                  tags: '@smoke',
                                ]
                            ],
                        ],
                        'stage acceptance testing': [
                            [
                                action: 'Behat.perform',
                                params: [
                                  testEnvironment: 'dev',
                                  tags: '@acceptance',
                                ]
                            ],
                        ],
                        'prod prepare': [
                            [
                                action: 'Druflow.dbBackupSite',
                                params: [
                                  fromEnvironment: 'test',
                                  db: 'aroq',
                                ]
                            ],
                        ],
                        'prod deploy': [
                            [
                                action: 'Druflow.deployFlow',
                                params: [
                                  deployFlowEnvironment: 'test',
                                  // Continuos deployment.
                                  //deployFlowConfirm: [
                                  // environment: 'test',
                                  //  message: 'Deploy to Production?'
                                  //]
                                ]
                            ],
                        ],
                        'prod smoke testing': [
                            [
                                action: 'Behat.perform',
                                params: [
                                  testEnvironment: 'test',
                                  tags: '@smoke',
                                ]
                            ],
                        ],
                        'prod acceptance testing': [
                            [
                                action: 'Behat.perform',
                                params: [
                                  testEnvironment: 'test',
                                  tags: '@acceptance',
                                ]
                            ],
                        ],
                        'finalize': [
                            [
                                action: 'Publish.junit',
                            ],
                        ]
                    ]
            }
        }
    }
}
