#!/usr/bin/env bash
set -vx

# Add following files into local git ignore only
if [ "$1" == "local" ]; then
  if [ -f .git/info/exclude ]; then
    rm .git/info/exclude
  fi
  echo "info.yaml" > .git/info/exclude
  git update-index --assume-unchanged info.yaml
fi

composer require symfony/config:2.8 --prefer-dist -vvv

if [ "$1" == "local" ]; then
  echo "composer.json" >> .git/info/exclude
  echo "composer.lock" >> .git/info/exclude
  echo "vendor" >> .git/info/exclude
  git update-index --assume-unchanged composer.json
  git update-index --assume-unchanged composer.lock
  git update-index --assume-unchanged vendor
fi

set +vx

