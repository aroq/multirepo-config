#!/usr/bin/env bash
set -vx

# Add following files into local git ignore only
if [ "$1" == "local" ]; then
  rm -fR .git
else
  rm -f .gitignore
  cp -f .gitignore.docroot .gitignore
fi

rm -fR docroot/sites
cd docroot; ln -s ../code/common/sites; cd ..

set +vx

composer install
